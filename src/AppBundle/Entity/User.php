<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Please enter your name.")
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long.",
     *     groups={"Registration", "Profile"}
     * )
     *
     * @ORM\Column(name="name", type="string", length=127)
     */

    protected $name;



    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Post",mappedBy="author")
     */
    private $posts;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Post",mappedBy="likers")
     */
    private $likedPosts;


    /**
     * @ORM\ManyToMany(targetEntity="User",mappedBy="follows")
     */
    private $followers;

    /**
     * @ORM\ManyToMany(targetEntity="User",inversedBy="followers")
     * @ORM\JoinTable(name="followers",
     *     joinColumns={@ORM\JoinColumn(name="follower_id",referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="followed_id",referencedColumnName="id")}
     *     )
     */
    private $follows;


    public function __construct()
    {
        parent::__construct();
        $this->posts = new ArrayCollection();
        $this->likedPosts = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->follows = new ArrayCollection();
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param Post $post
     */
    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post)
    {
        $this->posts->add($post);
    }

    /**
     * @return ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Post $post
     */
    public function removeLikedPost(Post $post)
    {
        $this->likedPosts->removeElement($post);
    }

    /**
     * @param Post $post
     */
    public function addLikedPost(Post $post)
    {
        $this->likedPosts->add($post);
    }

    /**
     * @return ArrayCollection
     */
    public function getLikedPosts()
    {
        return $this->likedPosts;
    }

    /**
     * @return mixed
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @return mixed
     */
    public function getFollows()
    {
        return $this->follows;
    }

    /**
     * @param User $user
     */
    public function addFollowers(User $user)
    {
        $this->followers->add($user);
    }

    /**
     * @param User $user
     */
    public function addFollowed(User $user)
    {
        $this->follows->add($user);
    }


}

