<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * User controller.
 *
 * @Route("user")
 */
class UserController extends Controller
{
    /**
     * @Route("/", name="welcomePage")
     */
    public function indexAction()
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('indexNote');
        }
        return $this->render('@App/User/index.html.twig', array(// ...
        ));
    }


}
