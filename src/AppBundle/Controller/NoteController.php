<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Note;
use AppBundle\Entity\Post;
use AppBundle\Form\NoteType;
use AppBundle\Libs\Enumeration\Greetings;
use AppBundle\Libs\WelcomeMessage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * Note controller.
 *
 * @Route("note")
 */
class NoteController extends Controller
{
    /**
     * @Route("/", name="indexNote")
     * @Method({"GET","HEAD", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function indexAction(Request $request)
    {

        return $this->render('@App/Note/index.html.twig');
    }

}
