<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
//use Symfony\Component\Security\Core\Encoder\UserInterface;


/**
 *
 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
 */


class LoadArticleData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();

       $pass= password_hash("123321", PASSWORD_BCRYPT);

        $user
            ->setUsername('admin')
            ->setEmail('original@some.com')
            ->setPassword($pass)
            ->setName('administrator')
            ->setEnabled('1')
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($user);

        $manager->flush();
    }


}
